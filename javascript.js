// Generates random number 1 - 255 for RGB value
function RGB() {
    var rgb = Math.floor(Math.random() * 255);
    return rgb;
}

// Generates random number 1 - 180 for gradient degrees
function DEG() {
    var deg = Math.floor(Math.random() * 180);
    return deg;
}

// Set the 1st color of gradient
// Return RGB value of color one
function setColorOne() {
    var get = document.getElementById("color1");
    var colorOne = RGB() + "," + RGB() + "," + RGB();
    get.style.background = "rgba(" + colorOne + ")";
    return colorOne;
}

// Set the 2nd color of gradient
// Return RGB value of color two
function setColorTwo() {
    var get = document.getElementById("color2");
    var colorTwo = RGB() + "," + RGB() + "," + RGB();
    get.style.background = "rgba(" + colorTwo + ")";
    return colorTwo;
}

// Sets color to locked
function lock(id) {
	var get = document.getElementById(id);
	var attri = get.getAttribute("data-lock");
	
	if (attri === "off") {
		get.setAttribute("data-lock", "on");
		get.classList.add("locked");
	} else {
		get.setAttribute("data-lock", "off");
		get.classList.remove("locked");
	}
}

function setGrad() {
	
	var get = document.getElementById("gradient");
	var code = "";
	var codeText = document.getElementById("code");
	codeText.innerHTML = "";
	
	// If both colors are locked dont change gradient
	if (document.getElementById("color1").getAttribute("data-lock") === "on" && document.getElementById("color2").getAttribute("data-lock") === "on") {
		var col1 = document.getElementById("color1");
		var style1 = window.getComputedStyle(col1).getPropertyValue("background");
		var num1 = style1.length;
		var slice1 = style1.slice(4,num1);
		
		var col2 = document.getElementById("color2");
		var style2 = window.getComputedStyle(col2).getPropertyValue("background");
		var num2 = style2.length;
		var slice2 = style2.slice(4,num2);
		
		code = "linear-gradient(" + DEG() + "deg, rgba(" + slice1 +",1) 30%, rgba(" + slice2 + ",1) 100%)";
		// If 1st color is locked generate new second color and change gradient
	} else if (document.getElementById("color1").getAttribute("data-lock") === "on") {
		var col = document.getElementById("color1");
		var style = window.getComputedStyle(col).getPropertyValue("background");
		var num = style.length - 56;
		var slice = style.slice(4,num);
		code = "linear-gradient(" + DEG() + "deg, rgba(" + slice + ",1) 30%, rgba(" + setColorTwo() + ",1) 100%)";
		// If 2nd color is locked generate new 1st color and change gradient
	} else if (document.getElementById("color2").getAttribute("data-lock") === "on") {
		var col = document.getElementById("color2");
		var style = window.getComputedStyle(col).getPropertyValue("background");
		var num = style.length - 56;
		var slice = style.slice(4,num);
		code = "linear-gradient(" + DEG() + "deg, rgba(" + setColorOne() +",1) 30%, rgba(" + slice + ",1) 100%)";
		// If no color is locked generate new gradient
	} else {
		code = "linear-gradient(" + DEG() + "deg, rgba(" + setColorOne() +",1) 30%, rgba(" + setColorTwo() + ",1) 100%)";
	} 
	// Set gradient
	get.style.background=code;
	var codeElement = document.createElement("p");
	codeElement.append(code);
	codeText.append(codeElement);
};

function bottomGrad() {
	var get = document.getElementById("bottomHalf");
	var code = "linear-gradient(" + DEG() + "deg, rgba(" + RGB() + "," + RGB() + "," + RGB() + ",1) 30%, rgba(" + RGB() + "," + RGB() + "," + RGB() + ",1) 100%)";
	
	get.style.background = code;
};

bottomGrad();	
setGrad();